<img src="https://api.web.gamepix.com/assets/img/250/250/icon/guess-word.png" alt="Image" height="150" width="150">
<h1>Guess-telegram-bot</h1>
<h3>Description</h3>
<p>A simple bot for guessing the words, with the ability to save the rating of players, find the top of the best. Keep your personal statistics of guessed words. And more.</p>
<hr>
<h3>How start this bot?</h3>
<p>Create .env file with fields:</p>
<pre>
PORT = port for bot
TOKEN = bot token 
DB_URL = mongo db url
DOMAIN = set webhook for bot
REDIS_HOST = redis host
REDIS_PORT = redis port
</pre>
<p>Intall dependecies</p>
<pre>
npm install
</pre>
<p>Launch bot via command:</p>
<pre>
npm run start
</pre>
<h3>Technology stack</h3>
<p>Nodejs, MongoDb, Redis, Telegraf.js<p>