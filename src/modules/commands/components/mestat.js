const Users = require('../../../database/mongo/schema/user');
const {saveNewUser} = require("../../../database/mongo/scripts/add");

const meStatCommand = async (ctx) => {
    const userid = ctx.message.from.id;
    const user = ctx.message.from;

    try {
        const user = await Users.findOne({'about.id' : userid});

        if (!user) {
            await saveNewUser(user);
        }

        await ctx.reply(`Имя: ${user.about.first_name}\nОтгадал(а): ${user.data.guessed}\nБыл ведущим: ${user.data.leading}\nЛайки: ${user.data.likes}\n\nДата: ${user.date}`)

    } catch (e) {
        ctx.reply(e.message)
    }
}

module.exports = meStatCommand;