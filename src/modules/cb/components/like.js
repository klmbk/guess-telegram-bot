const Users = require('../../../database/mongo/schema/user')
const keyboards = require('../../../constants/keyboards')
const client = require('../../../database/redis/index')

const likeBlockDuration = 120000;

const likePresnter = async (ctx, {userid, data}) => {
    
    const leading_id = data.split('_')[1]
  
    if (userid === Number(leading_id)) {
         return await ctx.answerCbQuery('Вы не можете поставить себе лайк!', {show_alert: true})   
    }

    let userCached = JSON.parse(await client.get(leading_id));
     
    if (!userCached) {
        await client.set(leading_id, JSON.stringify({time: +new Date(), users: []}))
        userCached = JSON.parse(await client.get(leading_id));
    }
     
    if (userCached.users.includes(userid) ) {
        if ((+new Date() - userCached.time) > likeBlockDuration) {
            userCached.users = userCached.users.filter((item) => item !== userid)
            await client.set(leading_id, JSON.stringify(userCached))

            if (userCached.users.length === 0) {
                await client.del(leading_id)
            }
        };
        return await ctx.answerCbQuery('Лайк можно ставить раз в 2 минуты!', {show_alert: true})
    }

    const user = await Users.findOneAndUpdate({'about.id' : Number(leading_id)}, {$inc: {'data.likes': 1}}, {new: true})
    await ctx.editMessageReplyMarkup({inline_keyboard: keyboards.REPLAY(user.data.likes, user.about.id)})
    userCached.users.push(userid);
    await client.set(leading_id, JSON.stringify(userCached));
}


module.exports = likePresnter;