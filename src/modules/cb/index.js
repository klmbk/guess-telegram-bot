const likePresnter = require('./components/like');
const cb = require('../../constants/cb');
 
const checkCallbackData = async (ctx, params) => {
    try {
        if (params.data.includes(cb.LIKE)) {
            return await likePresnter(ctx, {...params});
        }
    }catch (e) {
        ctx.reply(e.message);
    }
}


module.exports = checkCallbackData