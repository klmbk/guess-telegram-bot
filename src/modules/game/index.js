const startGame = require('./components/start.game');
const checkGuess = require('./components/check.guess');
const gameController = require('./components/controll.game');
const {changeWordTrigger, changeWord} = require('./components/change.word');
 
module.exports = {
    startGame, 
    checkGuess, 
    gameController,
    changeWordTrigger,
    changeWord
};