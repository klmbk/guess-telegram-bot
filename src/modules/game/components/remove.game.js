const client = require('../../../database/redis/index');
const messages = require('../../../constants/messages');

const gameDuration = 600000;

//Deleting a game by time if no one guessed the word
const removeGameByTime = (ctx, {chatid, session_id}) => {
    setTimeout(() => {
        client.get(chatid).then((chat) => {
            if (!chat) return;

            const content = JSON.parse(chat);

            if (content.session_id === session_id) {
                client.del(chatid);
                ctx.deleteMessage(content.messageid);
                ctx.reply(messages.GAME_END + `*${content.word}*`, {parse_mode: 'Markdown'});
            }
        })
    }, gameDuration)
}

const checkGameDuration = (ctx, chat) => {
    const content = JSON.parse(chat);
    const gameSeconds = Math.ceil((+new Date() - content.time))
    

    if (gameSeconds > gameDuration) {
        client.del(content.chatid);
        ctx.deleteMessage(content.messageid);

        return false;
    }

    return true;
}


module.exports.removeGameByTime = removeGameByTime;
module.exports.checkGameDuration = checkGameDuration;