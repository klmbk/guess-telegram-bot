const client = require('../../../database/redis/index');
const messages = require('../../../constants/messages');

const changeWordTrigger = async (ctx, leading) => {
    const userid = ctx.message.from.id;
    const chatid = leading[1];
    try {
         const chat = await client.get(chatid);

         if (!chat) throw new Error(messages.GAME_NOT_START);

         const content = JSON.parse(chat);

         if (content.leading === userid && content.chatid === chatid.toString()) {
             content.changeWord = true;

             await client.set(chatid, JSON.stringify(content));

             return await ctx.reply(`${chatid}\n\n${messages.WRITE_YOUR_WORD}\nТекущее слово: ${content.word}`);
         }
         await ctx.reply(messages.ONLY_LEADING_CHANGE)
    } catch (e) {
        ctx.reply(e.message)
    }
}

const changeWord = async (ctx) => {
    const text = ctx.message.text.toLowerCase();
    const chatid = ctx.message.reply_to_message.text.split('\n')[0];
    const userid = ctx.message.from.id;

    try {
        const chat = await client.get(chatid)

        if (!chat) return new Error(messages.GAME_NOT_START);

        if (text.length > 100) throw new Error(messages.WORD_LENGHT_ERROR);

        const content = JSON.parse(chat);

        if (content.changeWord) {
            if (content.leading === Number(userid) && content.chatid === chatid) {
                content.changeWord = false;
                content.prew_words.push(content.word)
                content.word = text.replace(/[^a-zа-яё\s]/gi, '').replace(/\s+/g, '');
    
                await client.set(chatid, JSON.stringify(content));
    
                return await ctx.reply(`Слово успешно изменено на: ${content.word}`);
            }

            await ctx.reply(messages.ONLY_LEADING_CHANGE)
        }
        
    } catch (e) {
        ctx.reply(e.message)
    }
}


module.exports.changeWordTrigger = changeWordTrigger;
module.exports.changeWord = changeWord;