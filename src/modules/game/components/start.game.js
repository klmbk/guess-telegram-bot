const uuid = require('uuid');
const client = require('../../../database/redis/index');
const messages = require('../../../constants/messages');
const keyboards = require('../../../constants/keyboards');
const getRandomWord = require("../scripts/word");
const Users = require('../../../database/mongo/schema/user');
const {removeGameByTime, checkGameDuration} = require('./remove.game');

//Function to start a word game
const startGame = async (ctx, {chatid, userid, username, messageid}) => {
    const userlink = `[${username}](tg://user?id=${userid})`
    const session_id = uuid.v4()

    try {
        if (messageid) await ctx.deleteMessage(messageid);

        const chat = await client.get(chatid);

        if (chat && checkGameDuration(ctx, chat)) throw new Error(messages.GAME_ALREADY)

        const {message_id} = await ctx.reply(`${userlink} Объясняет слово 📖`, {
            parse_mode: 'Markdown',
            reply_markup: {inline_keyboard: keyboards.CONTROLLER(`yourword_${chatid}`)}
        })

        await Users.updateOne({'about.id' : userid}, {$inc: {'data.leading': 1}})

        const newGame = {
            chatid,
            leading: userid,
            username,
            word: (await getRandomWord()).word.word.toLowerCase(),
            prew_words: [],
            session_id,
            messageid: message_id,
            time: +new Date(),
            changeWord: false
        }

        await client.set(chatid, JSON.stringify(newGame))

        removeGameByTime(ctx, {chatid, session_id});
    } catch (e) {
         ctx.reply(e.message)
    }
}


module.exports = startGame;