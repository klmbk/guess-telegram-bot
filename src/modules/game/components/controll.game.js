const client = require('../../../database/redis/index');
const cb = require('../../../constants/cb');
const checkCallbackData = require('../../cb/index');
const startGame = require('./start.game');
const getRandomWord = require('../scripts/word');
const messages = require('../../../constants/messages');
const keyboards = require('../../../constants/keyboards');

//Checking for pressing game buttons
const gameController = async (ctx, {data, userid, messageid, chatid, username}) => {

    const keyboardLength = ctx.callbackQuery.message.reply_markup.inline_keyboard.length;
 
    try {
        const chat = await client.get(chatid);
        const content = JSON.parse(chat);
 
         switch (data) {
             case cb.VIEW_WORD:
 
                 if (!chat || !content) return;
 
                 if (content.leading !== userid) {
                     return await ctx.answerCbQuery(messages.NOT_FOR_YOU, {show_alert: true});
                 }
 
                 await ctx.answerCbQuery(`Объясните слово: ${content.word}`, {show_alert: true});
                 break
             case cb.CHANGE_WORD:
                 if (!chat || !content) return;

                 if (content.leading !== userid) {
                     return  await ctx.answerCbQuery(messages.NOT_FOR_YOU, {show_alert: true});
                 }

                 content.prew_words.push(content.word);
                 content.word = (await getRandomWord()).word.word.toLowerCase();
 
                 await client.set(chatid, JSON.stringify(content))

                 if (content.prew_words.length && keyboardLength < 4) {
                     await ctx.editMessageReplyMarkup({inline_keyboard: keyboards.CONTROLLER(`yourword_${chatid}`, true)})
                 }
 
                 await ctx.answerCbQuery(`Новое слово: ${content.word}`, {show_alert: true});
                 break
            case cb.PREV_WORD:
                if (!chat || !content) return;

                if (content.leading !== userid) {
                    return  await ctx.answerCbQuery(messages.NOT_FOR_YOU, {show_alert: true});
                }

                if (content.prew_words.length === 1) {
                    await ctx.editMessageReplyMarkup({inline_keyboard: keyboards.CONTROLLER(`yourword_${chatid}`)})
                }

                if (content.prew_words.length) {
                    content.word = content.prew_words[content.prew_words.length - 1];
                    content.prew_words.splice(content.prew_words.length - 1, 1);
                    
                    await client.set(chatid, JSON.stringify(content))

                    return await ctx.answerCbQuery(`Предыдущее слово: ${content.word}`, {show_alert: true});
                }

            break
             case cb.REPLAY:
                 startGame(ctx, {userid, username, chatid});
                 break
             default:
                 checkCallbackData(ctx, {userid, data})
         }
    } catch (e) {
        ctx.reply(e.message)
    }
 }


 module.exports = gameController;