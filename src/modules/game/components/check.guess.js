const client = require('../../../database/redis/index');
const Users = require('../../../database/mongo/schema/user')
const keyboards = require('../../../constants/keyboards');

//Checking a word for a correct match
const checkGuess = async (ctx, {text, userid, chatid, username}) => {
    const userlink = `[${username}](tg://user?id=${userid})`

    try {
        const chat = await client.get(chatid);

        if (!chat) return;

        const content = JSON.parse(chat);

        if (text === content.word && content.leading !== userid) {
            await client.del(chatid);
            await  ctx.deleteMessage(content.messageid);
            const user = await Users.findOne({'about.id' : content.leading})
            await ctx.reply(`${userlink} отгадал(а) слово: *${content.word}*\n\nЕсли вам понравилось как обьясняет ${content.username} можете поставить лайк!`, {
                parse_mode: 'Markdown',
                reply_markup: {inline_keyboard: keyboards.REPLAY(user.data.likes, user.about.id)}
            })
            await Users.updateOne({'about.id' : userid}, {$inc: {'data.guessed': 1}})
        }

        return;

    } catch (e) {
         ctx.reply(e.message)
    }
}

module.exports = checkGuess;