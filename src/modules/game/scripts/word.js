const urls = require('../../../constants/urls');
const request = require('request')

const getRandomWord = () => {
    return new Promise((res, rej) => {
        request.get(urls.WORD_HOST, {
            headers: {
                'Accept': 'application/json, text/javascript, */*; q=0.01'
            }
        }, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                rej(error)
            }

            res(JSON.parse(body))
        })
    })
}

module.exports = getRandomWord