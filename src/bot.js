const {Telegraf} = require('telegraf');
const keyboards = require('./constants/keyboards');
const messages = require('./constants/messages');
const formatContext = require('./scripts/ctx.format');
const {gameController, startGame, checkGuess, changeWord, changeWordTrigger} = require("./modules/game");
const {saveNewUser} = require("./database/mongo/scripts/add");
const {meStatCommand} = require("./modules/commands");
require('dotenv').config();
require('./database/mongo/index');

const domain = process.env.DOMAIN;
const port = process.env.PORT || 3000;
const token = process.env.TOKEN;
const bot = new Telegraf(token);

bot.start(  ctx => {
  
    if (ctx.message.chat.type === 'private') {
        if (ctx.message.text.includes('yourword')) {
          const leading = ctx.message.text.split('_')
          return changeWordTrigger(ctx, leading)
        }

       return ctx.reply(messages.START, {reply_markup: {inline_keyboard: keyboards.INVITE}})
    }

    if (ctx.message.text.includes('givemeadmin')) {
        return ctx.reply(messages.GIVE_ADMIN) 
    }

    startGame(ctx, {...formatContext(ctx)})
});

bot.command('mestat', meStatCommand)

bot.on('new_chat_members', ctx => {

    bot.telegram.getMe().then((me) => {
        if (me.id === ctx.message.new_chat_members[0].id) {
            return ctx.reply(messages.GIVE_ADMIN)
        }

        return ctx.reply(`${ctx.message.new_chat_members[0].first_name}, добро пожаловать! 👋`)
    })
})

bot.on('text', ctx => {
    if (ctx.message.chat.type === 'private' && ctx.message.reply_to_message) return changeWord(ctx);
    saveNewUser(ctx.message.from);
    checkGuess(ctx, {...formatContext(ctx)})
});

bot.on('callback_query', ctx => {
    if (ctx.callbackQuery.message.chat.type === 'private') return;
    gameController(ctx, {...formatContext(ctx)})
})

bot.launch(domain ?  {webhook: {port, domain}} : {allowedUpdates: true})
.then(() => console.log('Telegram bot started!'))
.catch((e) => console.log(`Telegram bot start with error: ${e}`))