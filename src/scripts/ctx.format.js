
const formatContext = (ctx) => {
   if (ctx.callbackQuery) {
       return {
        chatid: ctx.callbackQuery.message.chat.id.toString(),
        messageid:  ctx.callbackQuery.message.message_id,
        userid: ctx.callbackQuery.from.id,
        data: ctx.callbackQuery.data,
        username: `${ctx.callbackQuery.from.first_name} ${ctx.callbackQuery.from.last_name ? ctx.callbackQuery.from.last_name : ''}`
    }
   }

    return {
        chatid:  ctx.message.chat.id.toString(),
        chatType : ctx.message.chat.type,
        userid : ctx.message.from.id,
        text: ctx.message.text.toLowerCase(),
        messageid : ctx.message.message_id,
        username: `${ctx.message.from.first_name} ${ctx.message.from.last_name ? ctx.message.from.last_name : ''}`
  }
}



module.exports = formatContext;