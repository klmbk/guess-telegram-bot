module.exports = {
    VIEW_WORD: 'view_word',
    CHANGE_WORD: 'change_word',
    PREV_WORD: 'prev_word',
    REPLAY: 'replay',
    LIKE: 'like'
}