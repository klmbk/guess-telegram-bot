module.exports = {
    INVITE: [[
        {
            text: 'Добавить в группу',
            url: 'https://t.me/pandas_ro_bot?startgroup=givemeadmin'
        }
    ]],
    CONTROLLER: (params, prev) => {
        return [
            [
                {
                    text: 'Посмотреть слово',
                    callback_data: 'view_word'
                }
            ],
            [
                {
                    text: 'Новое слово',
                    callback_data: 'change_word'
                }
            ],
             prev ? [
                {
                    text: 'Предыдущее слово',
                    callback_data: 'prev_word'
                }
            ]: [],
            [
                {
                    text: 'Свое слово',
                    url: 'https://telegram.me/pandas_ro_bot?start=' + params
                }
            ]
        ]
    },
    REPLAY: (count, userid) => {
        return [
            [
                {
                    text: `${count} 👍`,
                    callback_data: `like_${userid}`
                }
            ],
            [
            {
                text: 'Хочу быть ведущий!',
                callback_data: 'replay'
            }
        ]
        ]
    }
}