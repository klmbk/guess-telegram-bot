const {model, Schema} = require('mongoose')


const Users = Schema({
    about: {
        id: {type: Number},
        first_name: {type: String},
        last_name: {type: String},
        username: {type: String},
        is_bot: {type: Boolean}
    },
    data: {
        leading: {type: Number},
        guessed: {type: Number},
        likes: {type: Number}
    },
    date: {type: String}
}, {versionKey: false})


module.exports = model('users', Users);