const {model, Schema} = require('mongoose')


const Chats = Schema({
    about: {
        id: {type: String},
        first_name: {type: String},
        last_name: {type: String},
        username: {type: String},
        is_bot: {type: String}
    },
    data: {
        leading: {type: Number},
        guessed: {type: Number}
    },
    date: {type: String}
}, {versionKey: false})


module.exports = model('chats', Chats);