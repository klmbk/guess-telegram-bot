const mongoose = require('mongoose')
require('dotenv').config()

const url = process.env.DB_URL;

const connect = async () => {
    try {
        await mongoose.connect(url);
    } catch (e) {
        throw  e
    }
}

connect().then(() => {})
.catch((e) => console.log(e))