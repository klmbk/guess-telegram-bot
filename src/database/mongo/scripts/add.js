const Users = require('../schema/user');

const saveNewUser = async (user) => {
    try {
        const candidate = await Users.findOne({'about.id' : user.id})

        if (candidate) {
            return
        }

        const newUser = new Users({
            about: user,
            data: {
                leading: 0,
                guessed: 0,
                likes: 0
            },
            date: new Date().toDateString()
        })

        await newUser.save()
    } catch (e) {
        return;
    }
}

module.exports.saveNewUser = saveNewUser;