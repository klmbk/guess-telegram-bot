const redis = require("redis");
require('dotenv').config();

const redisHost = process.env.REDIS_HOST;
const redisPort = process.env.REDIS_PORT;

const client = redis.createClient({
    socket: {
        host: redisHost,
        port: redisPort
    },
    password: 'UytAMGg4Upu5Oiq3JoqNuCh4OdmPOIBY'
});

const connectToRedis = async () => {
    try {await client.connect()} catch (e) {console.log(e)}
}

if (!client.isOpen) {
    connectToRedis()
}

module.exports = client;